package com.shinys;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;

@org.springframework.stereotype.Service
public class Service {
    private static final Logger logger = LoggerFactory.getLogger(Service.class);

    @Cacheable(cacheManager = "cache5m", cacheNames="product" , key = "#key")
    public String getBigString(String key){
        logger.warn("캐시 안먹고 이쪽으로 들어옴 key :{}", key);

        StringBuilder builder = new StringBuilder();
        for( int i = 0 ; i < 1000 ; i++) {
            builder.append(key);
        }

        //연산이 오래 걸리는 메소드라고 치자.
        try {
            Thread.sleep(4000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return builder.toString();
    }
}
