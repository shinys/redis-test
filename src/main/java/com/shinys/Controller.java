package com.shinys;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
public class Controller {

    private final Service service;

    @GetMapping(path = "test")
    public String test(
            @RequestParam(name = "key", defaultValue = "1")
            String key
    ){

        return service.getBigString(key);
    }
}
