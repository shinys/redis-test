package com.shinys;


import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.cache.RedisCacheWriter;
import org.springframework.data.redis.connection.RedisClusterConfiguration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.JdkSerializationRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializationContext;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentSkipListSet;

@Configuration
@EnableCaching
@EnableRedisRepositories  /* config.properties 레디스 정보 기입 후 주석해제 */
@PropertySource("classpath:application.properties")
public class RedisConfiguration {

    public static final List<CacheManager> cacheManagers = new ArrayList<>(10);

    @Autowired
    Environment env;

    @Bean
    public RedisConnectionFactory lettuceConnectionFactory(){

        RedisConnectionFactory redisConnectionFactory;
        if( "true".equalsIgnoreCase(env.getProperty("config.redis.is_cluster","false") )){
            //Redis Cluster  config.redis.cluster.nodes=host1:port1,host2:port2,host3:port3
            List<String> nodes = Arrays.asList(env.getProperty("config.redis.cluster.nodes").replaceAll("\\p{Z}","").split(","));
            redisConnectionFactory = new LettuceConnectionFactory(new RedisClusterConfiguration(nodes));
        }else{
            redisConnectionFactory = new LettuceConnectionFactory(env.getProperty("config.redis.host"),env.getProperty("config.redis.port",int.class));
        }
//        //Redis Sentinel
//        RedisSentinelConfiguration sentinelConfig = new RedisSentinelConfiguration().master(env.getProperty("config.redis.master"))
//                .sentinel(env.getProperty("config.redis.host1"),env.getProperty("config.redis.port1",int.class))
//                .sentinel(env.getProperty("config.redis.host2"),env.getProperty("config.redis.port2",int.class));
//        return new LettuceConnectionFactory(sentinelConfig);

        if(!"".equalsIgnoreCase( env.getProperty("config.redis.password","" ).replaceAll("\\p{Z}","") )){
            ((LettuceConnectionFactory) redisConnectionFactory).setPassword( env.getProperty("config.redis.password","" ).replaceAll("(^\\p{Z}+|\\p{Z}+$)","") );
        }

        return redisConnectionFactory;
    }

    @Bean("redisTemplate")
    public RedisTemplate<?, ?> redisTemplate() {
        Jackson2JsonRedisSerializer jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer(Object.class);
        jackson2JsonRedisSerializer.setObjectMapper(
                new ObjectMapper()
                        .enable(DeserializationFeature.USE_BIG_DECIMAL_FOR_FLOATS,
                                DeserializationFeature.USE_LONG_FOR_INTS,
                                DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT,
                                DeserializationFeature.ACCEPT_EMPTY_ARRAY_AS_NULL_OBJECT,
                                DeserializationFeature.READ_ENUMS_USING_TO_STRING,
                                DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
                        .enable(SerializationFeature.WRITE_ENUMS_USING_TO_STRING)
                        .enable(JsonGenerator.Feature.IGNORE_UNKNOWN)
                        .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
                        .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
                        .setSerializationInclusion(JsonInclude.Include.NON_NULL)
                        .setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY)
        );
        RedisTemplate<String, String> template = new RedisTemplate<String, String>();
        StringRedisSerializer stringRedisSerializer = new StringRedisSerializer();
        template.setKeySerializer(stringRedisSerializer);
        template.setHashKeySerializer(stringRedisSerializer);
        template.setValueSerializer(jackson2JsonRedisSerializer);
        template.setHashValueSerializer(jackson2JsonRedisSerializer);
        template.setConnectionFactory(lettuceConnectionFactory());
        return template;
    }

    @Bean("redisCacheTemplate")
    public RedisTemplate<?, ?> redisCacheTemplate() {
        RedisTemplate<String, String> template = new RedisTemplate<String, String>();
        StringRedisSerializer stringRedisSerializer = new StringRedisSerializer();
        //GenericJackson2JsonRedisSerializer jsonRedisSerializer = new GenericJackson2JsonRedisSerializer(new ObjectMapper());
        JdkSerializationRedisSerializer jdkSerializationRedisSerializer = new JdkSerializationRedisSerializer();
        template.setKeySerializer(stringRedisSerializer);
        template.setHashKeySerializer(stringRedisSerializer);
        template.setValueSerializer(jdkSerializationRedisSerializer);
        template.setHashValueSerializer(jdkSerializationRedisSerializer);
        template.setConnectionFactory(lettuceConnectionFactory());
        return template;
    }


    @Bean("cache5m")
    public RedisCacheManager CacheManagerFor5min() {
        RedisCacheManager manager = RedisCacheManager.RedisCacheManagerBuilder
                .fromCacheWriter(RedisCacheWriter.nonLockingRedisCacheWriter(lettuceConnectionFactory()))
                .cacheDefaults(
                        RedisCacheConfiguration.defaultCacheConfig()
                                .entryTtl(Duration.ofMinutes(5L))
                                .prefixKeysWith("APCP:")
                                .serializeKeysWith(RedisSerializationContext.SerializationPair.fromSerializer(new StringRedisSerializer()))
                                .serializeValuesWith(RedisSerializationContext.SerializationPair.fromSerializer(new JdkSerializationRedisSerializer()))
                )
                .initialCacheNames(
                        new ConcurrentSkipListSet<>(Arrays.asList("product"))
                )
                .disableCreateOnMissingCache()
                .build();
        cacheManagers.add(manager);
        return manager;
    }

}




